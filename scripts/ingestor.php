<?php
require __DIR__. '/../vendor/autoload.php';

$container = require __DIR__.'/../src/Geo/Container.php';
/**
 * -d (data) full path to the json file
 * -r top right coordinate in json
 * -l bottom left coordinate in json
 * -m time in epoch
 *
 */
$options = getopt("d:r:l:m:");

//Validate
if(empty($options['d'])){
    echo '-d (json file path) is required';
    exit;
}
if(empty($options['r']) || empty($options['l'])){
    echo "-r (top-right ) and -l (bottom left) coordinates are required (Eg -r {'lat' : 32.322, 'lng': 22})";
    exit;
}
if(empty($options['m']) || (strlen($options['m']) != 13) ){
    echo "-m must be  an 13-digits epoch time";
    exit;
}
try{
    $bottomLeft = json_decode($options['l'], true);
    $topRight = json_decode($options['r'], true);

}catch(\Exception $e){
    echo "can not decode coordinate json ";
    exit;
}
if(empty($bottomLeft['lat']) || empty($bottomLeft['lng']) || empty($topRight['lat']) || empty($topRight['lng'])){
    echo "missing coordinates";
    exit;
}

//Get the events
try{
    $eventsData = file_get_contents($options['d']);
    $eventsArr = json_decode($eventsData, true);
    if(is_array($eventsArr) && !isset($eventsArr[0])){
        $eventsArr = [$eventsArr];
    }
} catch(\Exception $e){
    echo '-d must be an absolute path to your VALID json file';
    exit;
}

// Geo-rect hash map [x][y]
$geoRectMap = [];

//Time slice hash map [timestamp]
$timeSliceMap = [];

//Active trips hash map [tripId]
$trips = [];

$eventService = $container['event'];
$tripService = $container['trip'];

//Process Events
$events = $eventService->createFromArray($eventsArr);
$eventService->ingest($events, $trips, $geoRectMap, $timeSliceMap);

//Queries

//how many trips passed through a geo-rect defined by two lat/lng points (ex. top­right, bottom­left)
//how many trips started or stopped within a geo-rect, and the sum total of their fares
//how many trips were occurring at a given point in time

$timeStart = microtime(true);
$results = $tripService->getTripCountByCoordinates($geoRectMap, $bottomLeft['lat'], $bottomLeft['lng'], $topRight['lat'], $topRight['lat']);
$timeResult =  $tripService->getTripCountByTime($timeSliceMap, $options['m']);
$timeEnd = microtime(true);
$timeSpent = round(($timeEnd - $timeStart) * 1000);

printf("Trip Count: %s \nStart/Stop Count: %s \nTotal Fare: $%s\n", $results['trip-cnt'], $results['start-stop-cnt'], number_format($results['fare']));
printf("Total trips occurring at a given point in time ( %s ): %s\n", $options['m'],$timeResult);
printf("Time Spent  : %s milliseconds\n", $timeSpent);
printf("Total events processed: %s\n\n",count($events));

