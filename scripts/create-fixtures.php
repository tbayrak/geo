<?php

/**
 * Options
 * -t number of trips (1-1000)
 * -e max nr of events per trip (10-100)
 * -c start coordinate in json ( Eg. {'lat' : 37.775562, 'lng' : -122.464974})
 * -m start time (epoch 13-digits)
 * -o full path to the output file

 */
$options = getopt("t:e:c:m:o:");

$nrOfTrips = intval($options['t']);
$maxNrOfEvents = intval($options['e']);
if(($nrOfTrips < 1) || ($nrOfTrips > 1000)){
    $nrOfTrips = 100;
}
if(($maxNrOfEvents < 10) || ($maxNrOfEvents > 100)){
    $maxNrOfEvents = 10;
}

$trips = [];
$events = [];

try{
    $coordinate = json_decode($options['c'], true);
}catch(\Exception $e){
    echo "can not decode coordinate json ";
    exit;
}
if(empty($coordinate['lat']) || empty($coordinate['lng'])){
    echo "missing coordinates";
    exit;
}

for($i = 1; $i<=$nrOfTrips; $i++){

    $epoch = $options['m'];

    $lat = $coordinate['lat'];
    $lng = $coordinate['lng'];

    $nrOfEvents = rand(10, $maxNrOfEvents);
    for($j = 0; $j<$nrOfEvents ; $j++){
        $epoch += rand(10000,99999);
        $lat += (rand(1,10) / 10000);
        $lng += (rand(1,10) / 10000);
        $event = [
            'tripId' => $i,
            'lat' => $lat,
            'lng' => $lng,
            'epoch' => $epoch,
        ];
        if($j == 0){
            $event['event'] = 'begin';
        } else if($j == ($nrOfEvents -1)){
            $event['event'] = 'end';
            $event['fare'] = rand(10,100);
        } else{
            $event['event'] = 'update';
        }
        $events[] = $event;
    }
}

file_put_contents($options['o'], json_encode($events));