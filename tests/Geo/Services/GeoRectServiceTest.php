<?php

use Geo\Services\GeoRectService;
use Geo\Models\GeoRect;

class GeoRectServiceTest extends \PHPUnit_Framework_TestCase{

    const COEFFICIENT = 1000;

    private static $geoRectService;

    public static function setUpBeforeClass(){
        self::$geoRectService = new GeoRectService(self::COEFFICIENT);
    }
    /**
     * @dataProvider coordinateProvider
     */
    public function testGetGeoRectIndexCoordinate($coefficient, $coordinate, $expected){
        $geoRectService = new GeoRectService($coefficient);
        $result = $geoRectService->getGeoRectIndexCoordinate($coordinate);
        $this->assertEquals($expected, $result);
    }

    public function testAddTripToMap(){
        $map = [];
        $tripId = 3;
        $x = 5;
        $y = 7;
        self::$geoRectService->addTripToMap($map, $tripId, $x, $y);

        $this->assertFalse(empty($map[$x][$y]));
        $this->assertInstanceOf('Geo\\Models\\GeoRect', $map[$x][$y]);
        $this->assertCount(1, $map[$x][$y]->getTrips());
        $this->assertEquals(reset($map[$x][$y]->getTrips()), $tripId);
    }

    /**
     * @depends testAddTripToMap
     */
    public function testGetGeoRectTotals(){
        $map = [];
        self::$geoRectService->addTripToMap($map, 1, 122123, 37045);
        $map[122123][37045]->addTripAnchor(1);
        $map[122123][37045]->addFare(1, 100);

        self::$geoRectService->addTripToMap($map, 1, 122444, 37100);
        $map[122444][37100]->addTripAnchor(1);
        $map[122444][37100]->addFare(1, 100);

        self::$geoRectService->addTripToMap($map, 2, 122444, 37100);

        $totals = self::$geoRectService->getGeoRectTotals($map, 122.100, 37.000, 122.500, 37.300);
        $this->assertEquals(2, $totals['trip-cnt']);
        $this->assertEquals(1, $totals['start-stop-cnt']);
        $this->assertEquals(100, $totals['fare']);

    }

    public function coordinateProvider(){
        return [
            [1000, 122.12345, 122123],
            [1000, -37.0453, -37045],
            [10, -37.0453, -370],
            [100, -37.0453, -3705],
        ];
    }

}