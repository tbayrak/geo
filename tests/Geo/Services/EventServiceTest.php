<?php

use Geo\Services\EventService;
use Geo\Models\Event;
use Geo\Services\TimeSliceService;
use Geo\Services\GeoRectService;
use Geo\Services\TripService;

class EventServiceTest extends \PHPUnit_Framework_TestCase{

    const COEFFICIENT = 1000;

    private static $eventService;

    private $trips;
    private $geoRectMap;
    private $timeSliceMap;

    public static function setUpBeforeClass(){

        $timeSliceService = new TimeSliceService('min');
        $geoRectService = new GeoRectService(1000);
        $tripService = new TripService($geoRectService, $timeSliceService);
        self::$eventService = new EventService($timeSliceService, $geoRectService, $tripService);
    }

    public function testIngestOneBegin()
    {
        $begin = [
          'event' => 'begin',
          'tripId' => 134,
          'lat' => 37.79947,
          'lng' => -122.511635,
          'epoch' => 1392864673030,
        ];

        $event = new Event($begin);
        $trips = [];
        $geoRectMap = [];
        $timeSliceMap = [];

        self::$eventService->ingestOne($event, $trips, $geoRectMap, $timeSliceMap);

        $this->assertCount(1, $trips);
        $this->assertInstanceOf('Geo\Models\Trip', $trips[134]);
        $this->assertFalse(empty($geoRectMap[37799][-122512]));
        $this->assertInstanceOf('Geo\Models\GeoRect', $geoRectMap[37799][-122512]);
        $this->assertCount(1, $geoRectMap[37799][-122512]->getTrips());
        $this->assertCount(1, $geoRectMap[37799][-122512]->getTripAnchors());
        $this->assertCount(0, $geoRectMap[37799][-122512]->getFares());
        $this->assertFalse(empty($timeSliceMap[23214411]));

        return ['trips' => $trips, 'geoRectMap' => $geoRectMap, 'timeSliceMap' => $timeSliceMap];
    }

    /**
     * @depends testIngestOneBegin
     */
    public function testIngestOneUpdate($results){
        $update = [
            'event' => 'update',
            'tripId' => 134,
            'lat' => 37.81947,
            'lng' => -122.500635,
            'epoch' => 1392864800000,
        ];
        $event = new Event($update);
        self::$eventService->ingestOne($event, $results['trips'], $results['geoRectMap'], $results['timeSliceMap']);

        $this->assertCount(1, $results['trips']);
        $this->assertCount(1, $results['geoRectMap'][37799][-122512]->getTrips());
        $this->assertCount(0, $results['geoRectMap'][37799][-122512]->getFares());
        $this->assertCount(2, $results['timeSliceMap']);
        $this->assertFalse(empty($results['timeSliceMap'][23214413]));
        return $results;
    }

    /**
     * @depends testIngestOneUpdate
     */
    public function testIngestOneEnd($results){
        $end = [
            'event' => 'end',
            'tripId' => 134,
            'lat' => 37.82947,
            'lng' => -122.521635,
            'epoch' => 1392865400000,
            'fare' => 200
        ];
        $event = new Event($end);
        self::$eventService->ingestOne($event, $results['trips'], $results['geoRectMap'], $results['timeSliceMap']);

        $this->assertCount(0, $results['trips']);
        $this->assertCount(1, $results['geoRectMap'][37799][-122512]->getTrips());
        $this->assertCount(1, $results['geoRectMap'][37799][-122512]->getFares());
        $this->assertEquals(200, reset($results['geoRectMap'][37799][-122512]->getFares()));
        $this->assertCount(3, $results['timeSliceMap']);
    }

    public function testCreateFromArray(){
        $eventArr = [
            [
                'event' => 'begin',
                'tripId' => 134,
                'lat' => 37.79947,
                'lng' => -122.511635,
                'epoch' => 1392864673030,
            ], [
                'event' => 'update',
                'tripId' => 134,
                'lat' => 37.81947,
                //lng missing
                'epoch' => 1392864800000,
            ],[
                'event' => 'end',
                'tripId' => 134,
                'lat' => 37.82947,
                'lng' => -122.521635,
                'epoch' => 1392865400000,
                'fare' => 200
            ]
        ];
        $events = self::$eventService->createFromArray($eventArr);
        $this->assertCount(2, $events);
        $this->assertInstanceOf('Geo\Models\Event', $events[0]);

    }


}