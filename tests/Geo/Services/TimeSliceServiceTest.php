<?php

use Geo\Services\TimeSliceService;
use Geo\Models\TimeSlice;

class TimeSliceServiceTest extends \PHPUnit_Framework_TestCase{

    const TIME_UNIT = 'sec';

    private static $timeSliceService;

    public static function setUpBeforeClass(){
        self::$timeSliceService = new TimeSliceService(self::TIME_UNIT);
    }

    public function testAddTripToMap(){
        $map = [];
        $tripId = 3;
        $timeSlice = 5;
        self::$timeSliceService->addTripToMap($map, $tripId, $timeSlice);

        $this->assertFalse(empty($map[$timeSlice]));
        $this->assertInstanceOf('Geo\\Models\\TimeSlice', $map[$timeSlice]);
        $this->assertCount(1, $map[$timeSlice]->getTrips());
        $this->assertEquals(reset($map[$timeSlice]->getTrips()), $tripId);
    }

    /**
     * @dataProvider timeProvider
     */
    public function testConvertFromEpoch($timeUnit, $epoch, $expected){
        $timeService = new TimeSliceService($timeUnit);
        $result = $timeService->convertFromEpoch($epoch);
        $this->assertEquals($expected, $result);
    }

    /**
     * @depends testAddTripToMap
     */
    public function testGetTripCountByTime(){
        $map = [];
        $tripId = 3;
        $timeSlice = 1433223045000;
        self::$timeSliceService->addTripToMap($map, $tripId, 1433223045);
        $cnt = self::$timeSliceService->getTripCountByTime($map, $timeSlice);
        $this->assertFalse(empty($map[1433223045]));
        $this->assertEquals(1, $cnt);
    }

    public function timeProvider(){
        return [
            ['sec', 1433223045000 , 1433223045],
            ['min', 1433223045000 , 23887050],
            ['hour', 1433223045000 , 398117],
        ];
    }
}