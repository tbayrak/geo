<?php

use Pimple\Container;
use Geo\Services\EventService;
use Geo\Services\GeoRectService;
use Geo\Services\TripService;
use Geo\Services\TimeSliceService;

$container = new Container();

$container[ 'config' ] = require_once __DIR__.'/../../config/local.php';

$container['event'] = function ($c){
    return new EventService($c['time-slice'], $c['geo-rect'], $c['trip']);
};
$container['geo-rect'] = function ($c){
    return new GeoRectService($c['config']['geo-accuracy-coefficient']);
};
$container['trip'] = function ($c){
    return new TripService($c['geo-rect'], $c['time-slice']);
};
$container['time-slice'] = function ($c){
    return new TimeSliceService($c['config']['time-unit']);
};

return $container;