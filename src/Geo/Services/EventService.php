<?php

namespace Geo\Services;


use Geo\Models\Event;
use Geo\Models\Trip;

class EventService {

    private $timeSliceService;
    private $geoRectService;
    private $tripService;

    function __construct( $timeSliceService, $geoRectService, $tripService ) {
        $this->timeSliceService = $timeSliceService;
        $this->geoRectService = $geoRectService;
        $this->tripService = $tripService;
    }

    public function ingest($events, &$trips, &$geoRectMap, &$timeSliceMap){
        foreach($events as $event){
            try {
                $this->ingestOne($event, $trips, $geoRectMap, $timeSliceMap);
            }catch(\Exception $e){
                // 1. Log the exception
                // 2. Continue not to affect the rest of events
                continue;
            }
        }
    }

    public function ingestOne(Event $event, &$trips, &$geoRectMap, &$timeSliceMap){

        $timeSlice = $this->timeSliceService->convertFromEpoch($event->getEpoch());
        $geoRectX = $this->geoRectService->getGeoRectIndexCoordinate($event->getLat());
        $geoRectY = $this->geoRectService->getGeoRectIndexCoordinate($event->getLng());
        $tripId = $event->getTripId();

        switch($event->getCategory()){
            case 'begin':
                if(empty($trips[$tripId])){
                    $this->geoRectService->addTripToMap($geoRectMap, $tripId, $geoRectX, $geoRectY);
                    $geoRectMap[$geoRectX][$geoRectY]->addTripAnchor($tripId);
                    $this->timeSliceService->addTripToMap($timeSliceMap, $tripId, $timeSlice);
                    $trips[$tripId] = new Trip($tripId, $geoRectMap[$geoRectX][$geoRectY], $timeSliceMap[$timeSlice]);

                } else {
                    throw new \Exception('The trip has already started!');
                }
            break;
            case 'update':
                if(!empty($trips[$tripId])){
                    $this->geoRectService->addTripToMap($geoRectMap, $tripId, $geoRectX, $geoRectY);
                    $this->timeSliceService->addTripToMap($timeSliceMap, $tripId, $timeSlice);
                    $trips[$tripId]->setLatestEvent('update');
                } else {
                    throw new \Exception('A trip must be started before getting an update!');
                }

            break;
            case 'end':
                if(!empty($trips[$tripId])){
                    $this->geoRectService->addTripToMap($geoRectMap, $tripId, $geoRectX, $geoRectY);
                    $this->timeSliceService->addTripToMap($timeSliceMap, $tripId, $timeSlice);

                    $geoRectMap[$geoRectX][$geoRectY]->addTripAnchor($tripId);
                    $geoRectMap[$geoRectX][$geoRectY]->addFare($tripId, $event->getFare());

                    $startGeoRect = $trips[$tripId]->getStartGeoRect();
                    $startGeoRect->addFare($tripId, $event->getFare());

                    //Clean up memory .. (Assuming we won't get a trip begin message after it ends)
                    unset($trips[$tripId]);

                } else {
                    throw new \Exception('A trip must be started before getting an update!');
                }
            break;
            default:
                //Unknown event
                throw new \Exception('Unknown event ' . $event->getCategory());
            break;

        }

    }

    public function createFromArray($eventArr){
        $events = [];
        foreach($eventArr as $evnt){
            try {
                $events[] = new Event($evnt);
            }catch(\Exception $e){
                // 1. Log the exception
                // 2. Continue not to affect the rest of events
                continue;
            }
        }
        return $events;
    }

}