<?php

namespace Geo\Services;


class TripService {

    private $geoRectService;
    private $timeSliceService;

    function __construct( $geoRectService, $timeSliceService ) {
        $this->geoRectService = $geoRectService;
        $this->timeSliceService = $timeSliceService;
    }

    public function getTripCountByCoordinates($geoRectMap, $latLeft, $lngLeft, $latRight, $lngRight){
        return $this->geoRectService->getGeoRectTotals($geoRectMap, $latLeft, $lngLeft, $latRight, $lngRight);
    }
    public function getTripCountByTime($timeSliceMap, $epoch){
        return $this->timeSliceService->getTripCountByTime($timeSliceMap, $epoch);
    }

}