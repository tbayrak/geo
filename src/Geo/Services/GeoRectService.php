<?php

namespace Geo\Services;

use Geo\Models\GeoRect;

class GeoRectService {

    /**
     *
     * Accuracy level for the geo-rects
     *
     * @see config to change the
     *
     * @var $coefficient Int
     */
    private $coefficient;

    function __construct( $coefficient ) {
        $this->coefficient = $coefficient;
    }

    public function getGeoRectIndexCoordinate($coordinate){
        return intval(round($coordinate * $this->coefficient));
    }

    public function addTripToMap(&$geoRectMap, $tripId, $geoRectX, $geoRectY){
        if(empty($geoRectMap[$geoRectX][$geoRectY])){
            $geoRectMap[$geoRectX][$geoRectY] = new GeoRect();
        }
        $geoRectMap[$geoRectX][$geoRectY]->addTrip($tripId);
    }

    public function getGeoRectTotals($geoRectMap, $latLeft, $lngLeft, $latRight, $lngRight){
        if(($latLeft > $latRight) || ($lngLeft > $lngRight)){
            throw new \Exception("Not a valid geo rect");
        }
        $latLeft = $this->getGeoRectIndexCoordinate($latLeft);
        $lngLeft = $this->getGeoRectIndexCoordinate($lngLeft);
        $latRight = $this->getGeoRectIndexCoordinate($latRight);
        $lngRight = $this->getGeoRectIndexCoordinate($lngRight);

        $totals = [
            'trip-cnt' => 0 ,
            'start-stop-cnt' => 0,
            'fare' => 0,
        ];
        $tmpTrips = [];
        $tmpTripAnchors = [];
        $tmpFares = [];
        for($i=$latLeft; $i <= $latRight; $i++){
            for($j = $lngLeft; $j < $lngRight; $j++){
                if(!empty($geoRectMap[$i][$j])){
                    if($geoRectMap[$i][$j]->getTrips() && is_array($geoRectMap[$i][$j]->getTrips())){
                        $tmpTrips += $geoRectMap[$i][$j]->getTrips();
                    }
                    if($geoRectMap[$i][$j]->getTripAnchors() && is_array($geoRectMap[$i][$j]->getTripAnchors())){
                        $tmpTripAnchors += $geoRectMap[$i][$j]->getTripAnchors();
                    }
                    if($geoRectMap[$i][$j]->getFares() && is_array($geoRectMap[$i][$j]->getFares())){
                        $tmpFares += $geoRectMap[$i][$j]->getFares();
                    }

                }
            }
        }
        foreach($tmpFares as $fare){
            $totals['fare'] += $fare;
        }
        $totals['trip-cnt'] = count($tmpTrips);
        $totals['start-stop-cnt'] = count($tmpTripAnchors);
        return $totals;
    }

}