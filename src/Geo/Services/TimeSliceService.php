<?php
namespace Geo\Services;
use Geo\Models\TimeSlice;

class TimeSliceService {

    /**
     * Time
     * @var $timeUnit String
     */
    private $timeUnit;

    function __construct( $timeUnit ) {
        $this->timeUnit = $timeUnit;
    }

    /**
     * @return String
     */
    public function getTimeUnit() {
        return $this->timeUnit;
    }

    /**
     * @param String $timeUnit
     */
    public function setTimeUnit( $timeUnit ) {
        $this->timeUnit = $timeUnit;
    }

    public function convertFromEpoch($epoch){

        if(!is_numeric($epoch) || (strlen($epoch) != 13)){
            throw new \Exception('Epoch must be a 13-digits integer');
        }

        switch($this->timeUnit){
            case 'sec':
                return intval($epoch / 1000);
            break;
            case 'min':
                return intval($epoch / 60000);
            break;
            case 'hour':
                return intval($epoch / 3600000);
            break;
            default:
                throw new \Exception('Time unit must be sec, min or hour');
            break;
        }
    }

    public function addTripToMap(&$timeSliceMap, $tripId, $timeSlice){
        if(empty($timeSliceMap[$timeSlice])){
            $timeSliceMap[$timeSlice] = new TimeSlice();
        }
        $timeSliceMap[$timeSlice]->addTrip($tripId);
    }

    public function getTripCountByTime($timeSliceMap, $epoch){
        $timeInUnit = $this->convertFromEpoch($epoch);
        if(!empty($timeSliceMap[$timeInUnit]) && $timeSliceMap[$timeInUnit]->getTrips() && is_array($timeSliceMap[$timeInUnit]->getTrips()) ){
            return count($timeSliceMap[$timeInUnit]->getTrips());
        } else {
            return 0;
        }
    }
}