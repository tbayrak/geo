<?php

namespace Geo\Models;


class Event {
    /**
     * @var $tripId Int
     */
    private $tripId;

    /**
     * @var $category String
     */
    private $category;

    /**
     * @var $lat float
     */
    private $lat;

    /**
     * @var $lng float
     */
    private $lng;

    /**
     * @var $epoch Int
     */
    private $epoch;

    /**
     * @var $fare float
     */
    private $fare;

    function __construct( $event ) {
        if(!is_array($event) || count(array_diff(['event', 'tripId', 'lat', 'lng', 'epoch'], array_keys($event))) > 0){
            throw new \Exception('Invalid event construction');
        }
        $this->tripId = $event['tripId'];
        $this->category = $event['event'];
        $this->lat = $event['lat'];
        $this->lng = $event['lng'];
        $this->epoch = $event['epoch'];
        if(!empty($event['fare'])) {
            $this->fare = $event[ 'fare' ];
        }
    }

    /**
     * @return Int
     */
    public function getTripId() {
        return $this->tripId;
    }

    /**
     * @param Int $tripId
     */
    public function setTripId( $tripId ) {
        $this->tripId = $tripId;
    }

    /**
     * @return String
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param String $category
     */
    public function setCategory( $category ) {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getLat() {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat( $lat ) {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng() {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng( $lng ) {
        $this->lng = $lng;
    }

    /**
     * @return Int
     */
    public function getEpoch() {
        return $this->epoch;
    }

    /**
     * @param Int $epoch
     */
    public function setEpoch( $epoch ) {
        $this->epoch = $epoch;
    }

    /**
     * @return float
     */
    public function getFare() {
        return $this->fare;
    }

    /**
     * @param float $fare
     */
    public function setFare( $fare ) {
        $this->fare = $fare;
    }


}