<?php

namespace Geo\Models;


class GeoRect {

    /**
     * @var $trips Array
     */
    private $trips;

    /**
     * @var $fares Array
     */
    private $fares;

    /**
     * @var $tripAnchors Array
     */
    private $tripAnchors;

    function __construct() {
        $this->trips = [];
        $this->tripAnchors = [];
        $this->fares = [];
    }

    /**
     * @return Array
     */
    public function getTrips() {
        return $this->trips;
    }

    /**
     * @param Array $trips
     */
    public function setTrips( $trips ) {
        $this->trips = $trips;
    }

    public function tripExists ( $tripId ){
        return isset($this->trips[$tripId]);
    }

    public function addTrip ($tripId){
        if(!$this->tripExists($tripId)){
            $this->trips[$tripId] = true;
        }
    }

    /**
     * @return Array
     */
    public function getTripAnchors() {
        return $this->tripAnchors;
    }

    public function tripAnchorExists ( $tripId ){
        return isset($this->tripAnchors[$tripId]);
    }

    public function addTripAnchor ($tripId){
        if(!$this->tripAnchorExists($tripId)){
            $this->tripAnchors[$tripId] = true;
        }
    }

    /**
     * @return Array
     */
    public function getFares() {
        return $this->fares;
    }

    /**
     * @param Array $fares
     */
    public function setFares( $fares ) {
        $this->fares = $fares;
    }


    public function fareExists ( $tripId ){
        return isset($this->fares[$tripId]);
    }

    public function addFare ($tripId, $fare){
        if(!$this->fareExists($tripId)){
            $this->fares[$tripId] = $fare;
        }
    }

    /**
     * @param Array $tripAnchors
     */
    public function setTripAnchors( $tripAnchors ) {
        $this->tripAnchors = $tripAnchors;
    }


}