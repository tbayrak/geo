<?php

namespace Geo\Models;

class TimeSlice {

    /**
     * Array of trip ids
     * @var $trips Array
     */
    private $trips;

    function __construct(  ) {
        $this->trips = [];
    }

    /**
     * @return Array
     */
    public function getTrips() {
        return $this->trips;
    }

    /**
     * @param Array $trips
     */
    public function setTrips( $trips ) {
        $this->trips = $trips;
    }

    public function tripExists ( $tripId ){
        return isset($this->trips[$tripId]);
    }

    public function addTrip ($tripId){
        if(!$this->tripExists($tripId)){
            $this->trips[$tripId] = true;
        }
    }

}