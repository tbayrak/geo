<?php
namespace Geo\Models;


class Trip {

    /**
     * @var $id Int
     */
    private $id;

    /**
     * @var $fare float
     */
    private $fare;

    /**
     * @var $latestEvent string
     */
    private $latestEvent;

    /**
     * @var $startGeoRect GeoRect
     */
    private $startGeoRect;

    /**
     * @var $latestGeoRect GeoRect
     */
    private $latestGeoRect;

    /**
     * @var $endGeoRect Int
     */
    private $endGeoRect;

    /**
     * @var $startTimeSlice TimeSlice
     */
    private $startTimeSlice;

    /**
     * @var $latestTimeSlice TimeSlice
     */
    private $latestTimeSlice;

    /**
     * @var $endTimeSlice Int
     */
    private $endTimeSlice;

    function __construct( $id, $startGeoRect, $startTimeSlice ) {
        $this->id = $id;
        $this->startGeoRect = $startGeoRect;
        $this->startTimeSlice = $startTimeSlice;
        $this->latestEvent = 'begin';
    }

    /**
     * @return Int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param Int $id
     */
    public function setId( $id ) {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getFare() {
        return $this->fare;
    }

    /**
     * @param float $fare
     */
    public function setFare( $fare ) {
        $this->fare = $fare;
    }

    /**
     * @return string
     */
    public function getLatestEvent() {
        return $this->latestEvent;
    }

    /**
     * @param string $latestEvent
     */
    public function setLatestEvent( $latestEvent ) {
        $this->latestEvent = $latestEvent;
    }

    /**
     * @return Int
     */
    public function getStartGeoRect() {
        return $this->startGeoRect;
    }

    /**
     * @param Int $startGeoRect
     */
    public function setStartGeoRect( $startGeoRect ) {
        $this->startGeoRect = $startGeoRect;
    }

    /**
     * @return Int
     */
    public function getLatestGeoRect() {
        return $this->latestGeoRect;
    }

    /**
     * @param Int $latestGeoRect
     */
    public function setLatestGeoRect( $latestGeoRect ) {
        $this->latestGeoRect = $latestGeoRect;
    }

    /**
     * @return Int
     */
    public function getStartTimeSlice() {
        return $this->startTimeSlice;
    }

    /**
     * @param Int $startTimeSlice
     */
    public function setStartTimeSlice( $startTimeSlice ) {
        $this->startTimeSlice = $startTimeSlice;
    }

    /**
     * @return Int
     */
    public function getLatestTimeSlice() {
        return $this->latestTimeSlice;
    }

    /**
     * @param Int $latestTimeSlice
     */
    public function setLatestTimeSlice( $latestTimeSlice ) {
        $this->latestTimeSlice = $latestTimeSlice;
    }

    /**
     * @return Int
     */
    public function getEndTimeSlice() {
        return $this->endTimeSlice;
    }

    /**
     * @param Int $endTimeSlice
     */
    public function setEndTimeSlice( $endTimeSlice ) {
        $this->endTimeSlice = $endTimeSlice;
    }



}