<?php
$config = [
    'geo-accuracy-coefficient' => 100,
    'time-unit' => 'sec' // sec, min, hour
];
return $config;