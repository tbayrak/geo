### A demo app to process trip events and run some geospatial/time-based queries  ###

### Configuration (config/local.php) ###

* geo-accuracy-coefficient : A coefficient to convert geo-coordinates into 2d cartesian coordinates. It's 100 by default. Increasing it would yield more accurate results but poorer performance (see 'Test Results' below)
* time-unit : (sec|min|hour) Time unit conversions to improve the time-based query (see 'Test Results' below)

### Creating data fixtures ###

The script below creates random trip events and dumps them into a json file

Options:
 
 * -t number of trips  (1-1000)

 * -e max nr of events per trip (10-100)

 * -c start coordinate in json ( Eg. {'lat' : 37.775562, 'lng' : -122.464974})

 * -m start time (epoch 13-digits)

 * -o full path to the output file

Sample run:

```
#!bash

php ./scripts/create-fixtures.php -t 300 -e 50 -o "tests/fixtures/test2.json"  -c "{\"lat\": 37.799662,\"lng\": -122.46974}" -m 1392864800000

```
### Running unit tests ###
```
#!bash
vendor/bin/phpunit

```
### Running Queries ###

The script below will ingest the events (to mimic pub/sub) and run the queries.
Options:

 * -d full path to the json file to ingest

 * -r top right coordinate in json  (Eg. {'lat' : 37.775562, 'lng' : -122.464974})

 * -l bottom left coordinate in json

 * -m time in epoch (1392864800000)


Sample run:

```
#!bash

php ./scripts/ingestor.php -d "tests/fixtures/test.json" -r "{\"lat\": 37.799662,\"lng\": -122.46974}" -l "{\"lat\": 37.776662,\"lng\": -122.44374}" -m 1392865533354

```

### Test Results ###
```
#!bash
php ./scripts/ingestor.php -d "tests/fixtures/test.json" -r "{\"lat\": 37.799662,\"lng\": -122.46974}" -l "{\"lat\": 37.776662,\"lng\": -122.44374}" -m 1433156213484 
```

* Nr of total trips: 300

* Nr of total events : 9373

##### First run - (geo-accuracy-coefficient: 100 , time-unit: sec) #####

* Trip Count: 107 

* Start/Stop Count: 104 

* Total Fare: $5,361

* Total trips occurring at a given point in time ( 1433156213484 ): 247

* Time Spent  : 5 milliseconds

##### Second run (geo-accuracy-coefficient: 100 , time-unit: min) #####

* Time Spent  : 5 milliseconds

##### Third run (geo-accuracy-coefficient: 500 , time-unit: sec) #####

* Trip Count: 104 

* Start/Stop Count: 68 

* Total Fare: $3,582

* Total trips occurring at a given point in time ( 1433156213484 ): 300

* Time Spent  : 98 milliseconds

##### Fourth run (geo-accuracy-coefficient: 1000 , time-unit: sec) #####

* Trip Count: 91 

* Start/Stop Count: 55 

* Total Fare: $2,907

* Total trips occurring at a given point in time ( 1433156213484 ): 300

* Time Spent  : 375 milliseconds